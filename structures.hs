{-# LANGUAGE ExistentialQuantification #-}

module DEP.Structures where

import Data.Char
import Data.List
import Data.String
import Data.Function
import Text.Parsec
import Text.Parsec.String
import Control.Monad

data AnyShow = forall s. Show s => AS s
data Bit = F|D|T deriving (Eq)
data Label = Label String deriving (Eq)
data LabelBitSeq = LBS String BitSeq deriving (Eq)
data Tree a = Leaf a | Node (Tree a) (Tree a)
data Karnaugh = Karnaugh Label [Label] (Tree Bit)
type BitSeq = [Bit]
type CombTab = [(BitSeq,BitSeq)]
type Moore state input output = ([state],[input],state -> input -> state, state -> output)
type Mealy state input output = ([state],[input],state -> input -> state, state -> input -> output)
data Table a = Table [[a]]

executeParser :: String -> Either ParseError (Table AnyShow)
executeParser = parse parseTable ""

trim :: String -> String
trim = dropWhileEnd isSpace . dropWhile isSpace

parseTable :: Parser (Table AnyShow)
parseTable = do
	many $ try parseCommentLine
	x <- many1 parseLine 
	return $ Table x

parseLine :: Parser [AnyShow]
parseLine = do
	many $ try parseCommentLine
	x <- parseCellLine
	char '\n'
	return x

parseCommentLine :: Parser ()
parseCommentLine = do
	parseComment
	newline
	return ()

parseCellLine :: Parser [AnyShow]
parseCellLine = many1 (do
		optional $ char '|'
		parseValue)

parseComment :: Parser String
parseComment = many1.choice.map char $ "+-*"

parseValue :: Parser AnyShow
parseValue = do
				x <- many1 $ noneOf "|\n"
				let tx = trim x
				if all (`elem` "01TtFfXxdD-") tx then
					return.AS. map (readBit.toUpper) $ tx
				else 
					return.AS .Label $ tx

class DEPOut a where
    toAscii, toLaTeX, toSvg, toHtml :: a -> String
    toLaTeX x = "\\begin{verbatim}\n"++toAscii x++"\n\\end{verbatim}"
    toSvg = toHtml
    toHtml = toAscii
 
instance Show a => DEPOut (Table a) where
    toHtml (Table x) = showHTMLTable x
    toAscii (Table x) = showTable x

instance Show a => Show (Table a) where
    show (Table x) = showTable x
    
instance Show LabelBitSeq where
    show (LBS x y) = x++"/"++(show y)

splitKarnaugh :: Karnaugh -> (Karnaugh,Karnaugh)
splitKarnaugh (Karnaugh l (_:ls) (Node ta tb)) = (Karnaugh l ls ta,Karnaugh l ls tb)

showKarnaugh :: Karnaugh -> [String]
showKarnaugh (Karnaugh l ls t) = showKarnaugh' (length ls) ls t

showKarnaugh' :: Int -> [Label] -> Tree Bit -> [String]
showKarnaugh' 0 _ (Leaf x) = ["+---+","|   |","| "++show x++" |","|   |","+---+"]
showKarnaugh' k (Label la:ls) (Node ta tb) | odd k = zipwMap (++) (drop 1) (showKarnaugh' (k-1) ls ta) (showKarnaugh' (k-1) ls tb) --karnaughMarkerH 4 la++ (header)
                                           | otherwise = showKarnaugh' (k-1) ls ta ++ (drop 1 (showKarnaugh' (k-1) ls tb))

karnaughMarkerH :: Int -> String -> [String]
karnaughMarkerH n l = let m = length l in [replicate n ' '++('|':replicate (n-1) '-')++"|",replicate ((div (3*n) 2)-(div m 2)) ' '++l++replicate (2*n-(div (3*n) 2)-m+(div m 2)) ' ',replicate (2*n+1) ' ']

showHTMLTable :: (Show a) => [[a]] -> String
showHTMLTable [h] = "<table>\n<th><td>"++intercalatemap "</td><td>" show h++"</td></th>\n</table>"
showHTMLTable (h:t) = let f = intercalatemap "</td><td>" show in "<table>\n<th><td>"++f h++"</td></th>\n<tr><td>"++ intercalatemap "</td></tr>\n<tr><td>" f t++"</td></tr>\n</table>"

showTable :: (Show a) => [[a]] -> String
showTable (x:xs) = let len = measureTable (x:xs) in intercalate "\n" [showTableLine len x,showTableHead len,showTable'' xs len]

showTable' :: (Show a) => [[a]] -> String
showTable' x = showTable'' x (measureTable x)

showTable'' :: (Show a) => [[a]] -> [Int] -> String
showTable'' [] _ = "";
showTable'' list len = intercalate "\n" (map (showTableLine len) list)

showTableLine len x = intercalate " | " (zipWith tailingShow x len)

intercalatemap :: [a] -> (b -> [a]) -> [b] -> [a]
intercalatemap x f l = intercalate x (map f l)

zipwMap :: (a -> b -> c) -> (d -> b) -> [a] -> [d] -> [c]
zipwMap zf mf as ds = zipWith zf as (map mf ds)

showTableHead :: [Int] -> String
showTableHead len = intercalate "-+-" (map (`replicate` '-') len)

measureTable :: (Show a) => [[a]] -> [Int]
measureTable [] = []
measureTable [x] = map (length . show) x
measureTable (x:xs) = zipWith max (map (length . show) x) (measureTable xs)

tailingShow :: (Show a) => a -> Int -> String
tailingShow s n = let ss = show s in ss ++ replicate (n-length ss) ' '

instance Show AnyShow where
    show (AS a) = show a

instance Show Label where
    show (Label s) = s

instance Show Karnaugh where
    show = intercalate "\n" . showKarnaugh

instance Show Bit where
    show x = [showBit x]
    showList [] = id
    showList (x:xs) = \y -> show x ++ showList xs y

instance Read Bit where
    readsPrec 0 x = [(readBit (toUpper (head x)),"")]
    readList x = [(map (readBit.toUpper) x,"")]

showBit :: Bit -> Char
showBit F = '0'
showBit D = '-'
showBit T = '1'

readBit :: Char -> Bit
readBit '0' = F
readBit 'F' = F
readBit '-' = D
readBit 'D' = D
readBit 'X' = D
readBit '1' = T
readBit 'T' = T

canAssign :: Bit -> Bit -> Bool
canAssign _ D = True
canAssign x y = x == y

canAssignSeq :: BitSeq -> BitSeq -> Bool
canAssignSeq (x:_) [] = False
canAssignSeq [] (x:_) = False
canAssignSeq (x:xs) (y:ys) = canAssign x y && canAssignSeq xs ys

containsD :: BitSeq -> Bool
containsD = elem D

expandBS :: BitSeq -> [BitSeq]
expandBS [] = []
expandBS [D] = [[F],[T]]
expandBS [x] = [[x]]
expandBS (D:xs) = let tail = expandBS xs in map (\y -> F:y) tail ++ map (\y -> T:y) tail
expandBS (x:xs) = let tail = expandBS xs in map (\ys -> x:ys) tail

expandCT :: CombTab -> CombTab
expandCT [] = []
expandCT ((xa,xb):xs) = let ebs = expandBS xa in map (\y -> (y,xb)) ebs++expandCT xs

singleGroupBy :: (Ord b) => (a -> b) -> [a] -> [[a]]
singleGroupBy f = groupBy ((==) `on` f) . sortBy (compare `on` f)

elem2Index :: Eq a => [[a]] -> a -> Maybe Int
elem2Index = elem2Index' 0

elem2Index' :: Eq a => Int -> [[a]] -> a -> Maybe Int
elem2Index' _ [] _ = Nothing
elem2Index' i (x:xs) a | elem a x = Just i
                       | otherwise = elem2Index' (i+1) xs a

minimizeMealy :: (Eq state, Ord output) => Mealy state input output -> [[state]]
minimizeMealy (ss,ins,delta,f) = fixpoint (minimize2 ins delta) (minimize1Mealy ss ins f)

minimizeMoore :: (Eq state, Ord output) => Moore state input output -> [[state]]
minimizeMoore (ss,ins,delta,f) = fixpoint (minimize2 ins delta) (minimize1Moore ss f)

minimize1Moore :: (Ord output) => [state] -> (state -> output) -> [[state]]
minimize1Moore ss f = singleGroupBy f ss

minimize1Mealy :: (Ord output) => [state] -> [input] -> (state -> input -> output) -> [[state]]
minimize1Mealy ss ins f = singleGroupBy (\x -> map (f x) ins) ss

minimize2 :: (Eq state) => [input] -> (state -> input -> state) -> [[state]] -> [[state]]
minimize2 ins delta parts = concat [ singleGroupBy (\s -> map (elem2Index parts . delta s) ins) part | part <- parts ]

fixpoint :: (Eq a) => (a -> a) -> a -> a
fixpoint f x | x == y = x
             | otherwise = fixpoint f y
             where y = f x
