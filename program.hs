module Main where

import DEP.Structures
import System.Environment

main :: IO ()
main = print depheader

depheader :: String
depheader = "Cursus Digitale Elektronica en Processoren\nSoftware"
