program : *.hs
	ghc -o program program.hs structures.hs
install :
	make program
clean :
	rm *~ *.*~* *.o *.hi *.chi *.chs.h
